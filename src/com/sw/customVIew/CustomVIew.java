package com.sw.customVIew;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

@SuppressLint("DrawAllocation")
public class CustomVIew extends View {
	public float currentX = 40;
	public float currentY = 40;
	

	public float getCurrentX() {
		return currentX;
	}


	public void setCurrentX(float currentX) {
		this.currentX = currentX;
	}


	public float getCurrentY() {
		return currentY;
	}


	public void setCurrentY(float currentY) {
		this.currentY = currentY;
	}


	public CustomVIew(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		Paint myPaint = new Paint();
		myPaint.setColor(Color.BLUE);
		canvas.drawCircle(currentX, currentY, 15, myPaint);
	}
	

}
