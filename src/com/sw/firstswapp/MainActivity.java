package com.sw.firstswapp;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sw.customVIew.CustomVIew;


public class MainActivity extends Activity {
	
	 int currentImage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout layout = (RelativeLayout)findViewById(R.id.root);
        final CustomVIew subView = new CustomVIew(this);
        subView.setBackgroundColor(Color.RED);
        subView.setMinimumHeight(300);
        subView.setMinimumWidth(300);
        
        subView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				// TODO Auto-generated method stub
				
				subView.currentX = event.getX();
				subView.currentY = event.getY();
				subView.invalidate();
				
				return true;
			}
		});
        
        TextView subTextView = new TextView(this);
        subTextView.setText("hello world");
        
        
        layout.addView(subView);
        layout.addView(subTextView);
        
        
        
        
//        final ImageView myImageView = new ImageView(this);
//        layout.addView(myImageView);
//        myImageView.setImageResource(R.drawable.head);
//        myImageView.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				if (currentImage > 0) {
//					currentImage = 0; 
//					myImageView.setImageResource(R.drawable.test2);
//				}else {
//					currentImage = 1;
//					myImageView.setImageResource(R.drawable.head);
//				}
//			}
//		});
        
        
        Button clickBtnButton = (Button) findViewById(R.id.button1);
        clickBtnButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("out-sw");
			}
		});
        layout.removeView(clickBtnButton);
        clickBtnButton.setText("hello");
        layout.addView(clickBtnButton);
        
        
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
}
